package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * User: your name
 */
public class YourSolver implements Solver<Board> {

    private Dice dice;
    private Board board;
    private Point[] path;
    private int pathCounter = 1;
    private boolean recompute = false;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    @Override
    public String get(Board board) {
        this.board = board;

        if (board.isGameOver()) {
            path = null;
        }



        if (board.getSnake().size() == 0) return "";

        System.out.println(board.toString());

        Point apple = board.getApples().get(0);
        Point stone = board.getStones().get(0);
        List<Point> walls = board.getWalls();
        List<Point> snake = board.getSnake();
        Point head = board.getHead();

        System.out.println(snake);

        int[][] matrix = getMatrix(walls, snake, stone);

        printMatrix(matrix);

        if (path == null || recompute) {
            if (snake.size() <= 30) {
                path = BFS.findPath(matrix, head, apple);
                pathCounter = 1;
                recompute = false;
                if (path == null || path.length == 0) {
                    path = BFS.findPath(matrix, head, stone);
                    pathCounter = 1;
                    recompute = false;
                }
            } else {
                path = BFS.findPath(matrix, head, stone);
                pathCounter = 1;
            }
        }


        printSolution(path);

        String direction = "";

        if (path != null && pathCounter < path.length) {
            direction = getDirection(path != null ? path[pathCounter] : null, head);
            pathCounter++;
        } else {
            path = BFS.findPath(matrix, head, apple);
            pathCounter = 1;
            direction = getDirection(path != null ? path[pathCounter] : null, head);
            pathCounter++;
        }




        return direction;
    }


    public int[][] getMatrix(List<Point> walls, List<Point> snake, Point stone) {

        int[][] matrix = new int[board.size()][board.size()];

        walls.addAll(snake);

        walls.add(stone);

        List<Point> barriers = board.getBarriers();

        barriers.add(board.getHead());
        barriers.add(board.getStones().get(0));

        board.getBarriers().stream().forEach(e -> {
            matrix[e.getY()][e.getX()] = 1;
        });

        return matrix;
    }



    public String getDirection(Point next, Point head) {

        if (next == null) return "";

        if (next.getY() > head.getY()) {
            if (!board.getSnakeDirection().toString().equals("DOWN")) {
                return Direction.UP.toString();
            } else {
                recompute = true;

                if (next.getX() > head.getX()) {
                    return Direction.RIGHT.toString();
                } else {
                    return Direction.LEFT.toString();
                }
            }
        }

        if (next.getY() < head.getY()) {

            if (!board.getSnakeDirection().toString().equals("UP")) {
                return Direction.DOWN.toString();
            } else {
                recompute = true;
                if (next.getX() > head.getX()) {
                    return Direction.RIGHT.toString();
                } else {
                    return Direction.LEFT.toString();
                }
            }

        }

        if (next.getX() > head.getX()) {
            if (!board.getSnakeDirection().toString().equals("LEFT")) {
                return Direction.RIGHT.toString();
            } else {
                recompute = true;
                if (next.getY() > head.getY()) {
                    return Direction.UP.toString();
                } else {
                    return Direction.DOWN.toString();
                }
            }
        }

        if (next.getX() < head.getX()) {
            if (!board.getSnakeDirection().toString().equals("RIGHT")) {
                return Direction.LEFT.toString();
            } else {
                recompute = true;
                if (next.getY() > head.getY()) {
                    return Direction.UP.toString();
                } else {
                    return Direction.DOWN.toString();
                }
            }
        }

        return "";
    }




    void printMatrix(int[][] matrix) {
        for (int i = 0; i < board.size(); i++) {

            String row = "";

            for (int j = 0; j < board.size(); j++) {
                row += matrix[i][j];
            }

            System.out.println(row);
        }
    }


    void printSolution(Point[] path) {

        if (path != null) {
            for (int i = 0; i < path.length; i++) {
                System.out.print("[" + path[i].getX() + "," + path[i].getY() + "], ");
            }
        }

    }







    ////////newnewnewnewnewnew


    public static class BFS {

        private static final boolean DEBUG = false;

        static public Point[] findPath(final int[][] map,
                                final Point position,
                                final Point destination) {
//            if (isOutOfMap(map, position.getX(), position.getY())
//                    || isOutOfMap(map, destination.getX(), destination.getY())
//                    || isBlocked(map, position.getX(), position.getY())
//                    || isBlocked(map, destination.getX(), destination.getY())) {
//                return null;
//            }

            //todo: recomputeFlag



            Queue<Point> queue1 = new ArrayDeque<>();
            Queue<Point> queue2 = new ArrayDeque<>();

            queue1.add(position);

            //todo: change here
            map[position.getY()][position.getX()] = -1;

            for (int i = 2; !queue1.isEmpty(); i++) {
                if (queue1.size() >= map.length * map[0].length) {
                    throw new IllegalStateException("Map overload");
                }

                for (Point point : queue1) {
                    if (point.getX() == destination.getX() && point.getY() == destination.getY()) {
                        return arrived(map, i - 1, point);
                    }

                    final Queue<Point> finalQueue = queue2;
                    final int finalStepCount = i;

                    lookAround(map, point, (x, y) -> {
                        //todo: change here

                        if (isBlocked(map, y, x)) {
                            return;
                        }

                        Point e = new PointImpl(x, y);

                        finalQueue.add(e);

                        //todo: change here

                        map[e.getY()][e.getX()] = -finalStepCount;
                    });
                }

                if (DEBUG) {
                    printMap(map);
                }

                queue1 = queue2;
                queue2 = new ArrayDeque<>();
            }

            resetMap(map);

            return null;
        }

        private static boolean isOutOfMap(final int[][] map,
                                          final int x,
                                          final int y) {
            return x < 0 || y < 0 || map.length <= y || map[0].length <= x;
        }

        //todo: change
        private static boolean isBlocked(final int[][] map, final int y, final int x) {
            final int i = map[y][x];
            return i < 0 || i == 1;
        }

        private static Point[] arrived(final int[][] map, final int size, final Point p) {
            final Point[] optimalPath = new Point[size];

            computeSolution(map, p.getX(), p.getY(), size, optimalPath);

            resetMap(map);

            return optimalPath;
        }

        private static void resetMap(final int[][] map) {
            for (int y = 0; y < map.length; y++) {
                for (int x = 0; x < map[0].length; x++) {
                    if (map[y][x] < 0) {
                        map[y][x] = 0;
                    }
                }
            }
        }

        private static void printMap(final int[][] map) {
            for (final int[] r : map) {
                for (final int i : r) {
                    System.out.print(i + "\t");
                }

                System.out.println();
            }

            System.out.println("****************************************");
        }

        private static void computeSolution(final int[][] map,
                                     final int x,
                                     final int y,
                                     final int stepCount,
                                     final Point[] optimalPath) {
            if (isOutOfMap(map, x, y)
                    || map[y][x] == 0
                    || map[y][x] != -stepCount) {
                return;
            }

            final Point p = new PointImpl(x, y);

            optimalPath[stepCount - 1] = p;

            lookAround(map, p, (x1, y1) -> computeSolution(map, x1, y1, stepCount - 1, optimalPath));
        }

        private static void lookAround(final int[][] map,
                                final Point p,
                                final Callback callback) {
//            callback.look(map, p.getX() + 1, p.getY() + 1);
//            callback.look(map, p.getX() - 1, p.getY() + 1);
//            callback.look(map, p.getX() - 1, p.getY() - 1);
//            callback.look(map, p.getX() + 1, p.getY() - 1);
            callback.look(map, p.getX() + 1, p.getY());
            callback.look(map, p.getX() - 1, p.getY());
            callback.look(map, p.getX(), p.getY() + 1);
            callback.look(map, p.getX(), p.getY() - 1);
        }

         interface Callback {
            default void look(final int[][] map, final int x, final int y) {
                if (isOutOfMap(map, x, y)) {
                    return;
                }
                onLook(x, y);
            }

            void onLook(int x, int y);
        }
    }







    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                "http://46.101.112.224/codenjoy-contest/board/player/dvjfmqmh0uud93moy4ul?code=3199969337132773000",
                new YourSolver(new RandomDice()),
                new Board());
    }

}
